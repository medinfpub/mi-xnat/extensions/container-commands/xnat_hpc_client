from setuptools import setup

setup(
    name='hpcsera',
    version='0.1.0',
    py_modules=['hpcsera'],
    install_requires=[
        'Click',
        'Requests'
    ],
    entry_points={
        'console_scripts': [
            'hpcsera = hpcsera:cli',
        ],
    },
)